import java.util.Collections;
import java.util.Comparator;

//variables globales
int width = 500;
int height = 500;
Polygon2D figure;
ArrayList<Segment> segments;
//Listes des segments utilisés pour les algorithmes


void setup() {
  //Paramètres pour l'affichage 
  size(500, 500);
  noLoop();
  
  segments = new ArrayList<Segment>();
  figure = new Polygon2D();
  
  //Points triés dans le sens horaire
  
  /*figure.add(new Point(100,50));
  figure.add(new Point(200,50));
  figure.add(new Point(200,100));
  figure.add(new Point(150,200));
  figure.add(new Point(75,100));
  figure.add(new Point(150,125));
  figure.add(new Point(100,75));*/
  
  //Points triés dans le sens anti-horaire
  
  figure.add(new Point(100,75));
  figure.add(new Point(150,125));
  figure.add(new Point(75,100));
  figure.add(new Point(150,200));
  figure.add(new Point(200,100));
  figure.add(new Point(200,50));
  figure.add(new Point(100,50));
  
  ear_clipping();
  
}


void draw() { 
  background(255);
  for(int i=0;i<segments.size();++i)
    segments.get(i).draw(#ff0000);
  figure.draw(0);
  
  
  
}


public void ear_clipping(){
  
  //Il faut d'abord déterminer le sens d'orientation du polygone, qui sert ensuite à determiner si les triplets de vertices forment une oreille ou non
  //pour cela, il faut trouver le vertice avec le plus petit y de la figure (ou si plusieurs, celui qui est également le plus à droite)

 int min = 0;
 
 //On créée une copie de la liste des points du polygone pour les besoins de l'algo et on effectue en même temps du vertice pour l'orientation du polygone
 ArrayList<Point> copy = new ArrayList<Point>();
 for(int k=0;k<figure.pts.size();++k){
   copy.add(figure.pts.get(k));
   if(copy.get(k).y<copy.get(min).y || (copy.get(k).y==copy.get(min).y && copy.get(k).x > copy.get(min).x)){
     min = k;
   }
 }
 
 //On détermine à partir du signe du produit vectoriel des segments [min,min-1] et [min,min+1] si le polygone est dans le sens anti-horaire ou non
 Segment bmin = new Segment(copy.get(min),copy.get(Math.floorMod(min-1,copy.size())));
 Segment amin = new Segment(copy.get(min),copy.get(Math.floorMod(min+1,copy.size())));
 
 boolean clockwise = !(bmin.cross_product(amin)>0);



 Segment s;
 Segment s1;
 Segment sp;
 Segment normale;
 int dot00;
 int dot01;
 int dot02;
 int dot11;
 int dot12;
 double invDenom, u, v;
 int i=0;
 int j;
 int cpt,cpt2;
 boolean areInside,oriented;

cpt=0;
//On effectue le parcours cyclique tant qu'il reste plus de deux "oreilles" au polygone, cad plus de 3 sommets
 while(copy.size()> 3){


   s = new Segment(copy.get(Math.floorMod(i,copy.size())),copy.get(Math.floorMod(i-1,copy.size())));
   s1 = new Segment(copy.get(Math.floorMod(i,copy.size())),copy.get(Math.floorMod(i+1,copy.size())));
   
   /*Test si ear à enlever : 
   * si triangle formé est dirigé à l'intérieur du polygone et est donc une oreille potentielle
   * Pour le déterminer, on calcule un vecteur normal au segment s(rotation de 90° de ce dit vecteur) avec lequel on calcule le produit scalaire au second vecteur s1
   * si le polygone est dans le sens horaire et que le produit scalaire est >0 ou que le polygone est anti horaire et le produit scalaire <0
   * alors c'est une oreille potentielle à enlever
   */
   normale = new Segment(s.p1, s.coord_y, -s.coord_x);
   oriented = s1.dot_product(normale) >0;
   
   if((oriented && clockwise) || (!oriented && !clockwise)){
     

       //Dans ce cas, on ne peut la supprimer du polygone que si aucun autre point n'est situé à l'intérieur de cette dernière
       j=(i+2)%copy.size();
       dot00 = s.dot_product(s);
       dot01 = s.dot_product(s1);
       dot11 = s1.dot_product(s1);
       areInside=false;
       cpt2=0;
       //parcours de tous les autres points du polygone
       while(j!=Math.floorMod(i-1,copy.size()) && !areInside){
         
         //Ici est utilisé la méthode du barycentre pour determiner la collision entre un point et un triangle
          sp = new Segment(copy.get(Math.floorMod(i,copy.size())),copy.get(Math.floorMod(j,copy.size())));
          dot02 = s.dot_product(sp);
          dot12 = s1.dot_product(sp);
          
          invDenom = 1.0 / (dot00 * dot11 - dot01 * dot01);
          u = (double)(dot11 * dot02 - dot01 * dot12) * invDenom;
          v = (double)(dot00 * dot12 - dot01 * dot02) * invDenom;
          areInside= areInside || ((u >= 0) && (v >= 0) && (u + v < 1.0));
          j=Math.floorMod(j+1,copy.size());
          cpt2++;
          
       }
       //Si aucun n'est dedans, alors l'oreille peut être finalement retirée (cad le point i du tableau)
       if(!areInside){
         segments.add(new Segment(copy.get(Math.floorMod(i-1,copy.size())),copy.get(Math.floorMod(i+1,copy.size()))));
         copy.remove(i);
         i = 0;
         
       }
       //Sinon, on passe au point suivant
       else{
        i = Math.floorMod(i+1,copy.size()); 
       }
     
   }
   //Sinon, on passe au point suivant
   else{
    i = Math.floorMod(i+1,copy.size()); 
   }
   cpt++;
 }
 
  
}

class Polygon2D{
   ArrayList<Point> pts;
   
   public Polygon2D(){
     pts = new ArrayList<Point>();
   }
 
   public void draw(int clr){
     stroke (clr);
      for(int i=0;i<pts.size()-1;++i)
        line(pts.get(i).x,pts.get(i).y,pts.get(i+1).x,pts.get(i+1).y);
      line(pts.get(pts.size()-1).x,pts.get(pts.size()-1).y,pts.get(0).x,pts.get(0).y);
     noStroke();
   }
   
   public void add(Point p){
     pts.add(p);
   }
}


class Segment{
   Point p1;
   Point p2;
   int coord_x;
   int coord_y;
   
  
   public Segment(Point p1, Point p2){
     this.p1 = p1;
     this.p2 = p2;
     coord_x = p2.x - p1.x;
     coord_y = p2.y - p1.y;
   }
   
   public Segment(Point p, int coord_x, int coord_y){
      p1  =p;
      this.coord_x = coord_x;
      this.coord_y = coord_y;
      p2 = new Point(p.x+coord_x, p.y+coord_y);
   }
   
   
   /**
   * fonction qui affiche le segment à l'écran
   * @clr la couleur à utiliser pour dessiner le segment
   *
   */
   public void draw(int clr){
       stroke(clr);
       line(p1.x,p1.y,p2.x,p2.y);
       noStroke();
   }
  
   
   public int dot_product(Segment s){
     return (coord_x * s.coord_x) + (coord_y * s.coord_y);
   }
   
   public int cross_product(Segment s){
      return (coord_x * s.coord_y) - (s.coord_x * coord_y);
   }
   
   public String toString(){
      return "Segment Coord : "+coord_x+" , "+coord_y+" "+p1+" "+p2; 
   }
}

class Point{
   int x;
   int y;
   
   public Point(int x, int y){
     this.x = x;
     this.y = y;
   }
   
   public boolean equals(Point p){
    return x==p.x && y==p.y;
   }
   
   public void draw(int clr){
     stroke(clr);
     point(x,y);
     noStroke();
   }
   
   public String toString(){
    return ("("+x+","+y+")");
   }
  
}