import java.util.Collections;

int width = 500;
int height = 500;
ArrayList<Point> lp;
int timer;

// The statements in the setup() function 
// execute once when the program begins
void setup() {
  size(500, 500);  // Size must be the first statement
  noLoop();
  //generation des points
  generate(100000);
}
// The statements in draw() are executed until the 
// program is stopped. Each statement is executed in 
// sequence and after the last line is read, the first 
// line is executed again.
void draw() { 
  background(255);   // Clear the screen with a black background
  for(int i=0;i<lp.size();++i)
           lp.get(i).draw(0);
  findNearestBrut();
  //findNearestDNQ();
  findNearestSweepLine();
}

void generate(int nb){
    lp = new ArrayList<Point>();
    for(int i=0;i<nb;++i){
        lp.add(new Point((int)(random(width)),(int)random(height)));
    }
    timer = millis();
    Collections.sort(lp);
    timer= millis()-timer;
}

public void findNearestBrut(){
  System.out.println("\n\nAlgorithme BrutForce\n");
  int rtime=millis();
  Point[] res = calcBrut(0,lp.size());
  rtime= millis()-rtime;
  affRes(res[0],res[1],rtime);
}

public Point[] calcBrut(int i, int j){
  Point[] res = new Point[2];
  int p1=i;
  int p2=i+1;
  int lg=distance(lp.get(p1),lp.get(p2));
  int temp;
  for(int k=i;k<j-1;++k){
      for(int l=k+1;l<j;++l){
        temp = distance(lp.get(k),lp.get(l));
        if(lg>temp){
          p1 = k;
          p2 = l;
          lg=temp;
          break;
       }
      }
  }
  res[0]=lp.get(p1);
  res[1]=lp.get(p2);
  //System.out.println("Indices des points plus proches trouvés : "+p1+" "+p2);
  return res;
}

public void findNearestDNQ(){
   System.out.println("\n\nAlgorithme DNQ\n");
   int rtime = millis();
   Point[] res = calcRec(0,lp.size());
   rtime = millis()-rtime;
   affRes(res[0],res[1],rtime);
}

public Point[] calcRec(int i, int j){
  //System.out.println("[Calcul sur l'intervalle ["+i+","+j+"] ]");
  
   if((j-i)+1<100){
     //System.out.println("  L'intervalle est inférieur à 10, retourne le calcul brut.");
     return calcBrut(i,j);
   }
   
   int m = (i+j)/2;
   //System.out.println("  L'intervalle est supérieur à 10, traiment récursif.\n Le milieu de l'intervalle est : "+((i+j)/2));
   //System.out.println("  Début calcul de la paire à gauche pour l'intervalle["+i+","+j+"]");
   Point[] pl = calcRec(i,m);
   //System.out.println("  Fin calcul de la paire à gauche pour l'intervalle["+i+","+j+"]");
   //System.out.println("  Debut calcul de la paire à droite pour l'intervalle["+i+","+j+"]");
   Point[] pr = calcRec( m + 1 , j );
   //System.out.println("  Fin calcul de la paire à droite pour l'intervalle["+i+","+j+"]");
   int dl = distance(pl[0],pl[1]);
   int dr = distance(pr[0],pr[1]);
   int dist;
   Point[] res;
   if(dl> dr){
       dist = dr;
       res = pr;
   }
   else{
      dist = dl;
      res = pl;
   }
   //System.out.println("distance : "+dist);
    
   
    int lft, rght;
    lft = m-1;
    //System.out.println("left middle iterating");
    while((lp.get(m).x - lp.get(lft).x) < dist && lft>i){
        lft--;
        //System.out.println(lft);
    }
    lft++;
    rght=m+1;
    //System.out.println("right middle iterating");
    while((lp.get(rght).x - lp.get(m).x )< dist && rght<j-1){
      rght++;
      //System.out.println(rght);
    }
    rght--;
    //System.out.println("bla fin intervalle milieu, debut recherche\nPoints actuels : ("+res[0].x+","+res[0].y+")  ("+res[1].x+","+res[1].y+")");
    int temp;
    for(int k=lft;k<m;++k){
      for(int l=m+1;l<rght;++l){
        temp = distance(lp.get(k),lp.get(l));
        if(temp<dist){
          res[0]= lp.get(k);
          res[1]= lp.get(l);
          dist = temp;
        }
      }
    }
    //System.out.println("Points actuels après intervalle milieu : ("+res[0].x+","+res[0].y+")  ("+res[1].x+","+res[1].y+")");
    return res;
}

public void findNearestSweepLine(){
   System.out.println("\n\nAlgorithme SweepLine\n");
   int rtime = millis();
   Point[] res = sweepline(0,lp.size());
   rtime = millis()-rtime;
   affRes(res[0],res[1],rtime);
}

public Point[] sweepline(int i, int j){
 
  Point[] points = new Point[2];
  
  float dmin = dist(lp.get(i).x, lp.get(i).y, lp.get(i+1).x, lp.get(i+1).y);
  points[0] = lp.get(i+1);
  points[1] = lp.get(i+1);
  
  for(int l = 0; l < j; l++){
   
    for(int e = l+1; e < j; e++){
      
      if(abs(lp.get(e).x - lp.get(l).x) < dmin){
      
        float d = dist(lp.get(l).x, lp.get(l).y, lp.get(e).x, lp.get(e).y);
        
        if(dmin > d){
          dmin = d;
          points[0] = lp.get(l);
          points[1] = lp.get(e);
        }
      
      }
      
    }
    
  }
  
  return points;
  
} 

public void affRes(Point a, Point b, int rtime){
  
  stroke(#ff0000);
  line(a.x,a.y,b.x,b.y);
  noStroke();
  
  System.out.println("Points les plus proches : ("+a.x+","+a.y+")  ("+b.x+","+b.y+")\nTemps d'exécution : "+(timer+rtime)+" ms");
}

class Point implements Comparable{
   int x;
   int y;
   
   public Point(int x,int y){
     this.x = x;
     this.y = y;
   }
      
   public void draw(int clr){
       stroke(clr);
       int radius=2;
       /*for(int y=-radius; y<=radius; y++)
          for(int x=-radius; x<=radius; x++)
            if(x*x+y*y <= radius*radius)
              point(this.x+x, this.y+y);*/
       point(this.x, this.y);
       noStroke();
   }
   
   public int compareTo(Object obj){
      if(this==null || obj == null)
        throw new NullPointerException();
      if(obj instanceof Point){
        Point p = (Point) obj;
        if(this.x > p.x)
          return 1;
        else if(this.x<p.x)
          return -1;
        else{
          if(this.y > p.y)
            return 1;
          else if(this.y < p.y)
            return -1;
           else return 0;
        }
      }
      throw new ClassCastException();
   }
   
}

public static int distance(Point p1, Point p2){
     return abs((int)Math.pow(p2.y-p1.y,2)+(int)Math.pow(p2.x-p1.x,2));
}