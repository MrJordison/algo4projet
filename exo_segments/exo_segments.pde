import java.util.TreeMap;
import java.util.Map;
import java.util.Collections;
import java.util.Comparator;
import java.io.*;

//variables globales
int width = 500;
int height = 500;

//Listes des segments utilisés pour les algorithmes
ArrayList<Segment> seg_vert;
ArrayList<Segment> seg_horz;
ArrayList<Intersection> inters;
ArrayList<Intersection> intersFB;
ArrayList<Intersection> intersdnq;

void setup() {
  //Paramètres pour l'affichage 
  size(500, 500);
  noLoop();
  
  seg_vert = new ArrayList<Segment>();
  seg_horz = new ArrayList<Segment>();
  inters = new ArrayList<Intersection>();
  intersFB = new ArrayList<Intersection>();
  intersdnq = new ArrayList<Intersection>();
  
  //calcul des intersections
  //TODO placer les calculs de timers ici
  //génération des segments, avec paramètre indiquant le nombre à créer
  generate(10000,50,20);

  BrutForceAlgo();
  DNQAlgo();  
  sweepline();
    
  System.out.println("Intersection SweepLine : "+inters.size()+" points trouvés ; Brute Force : "+intersFB.size()+" points trouvés ; DNQ : "+intersdnq.size()+" points trouvés ; ");
  
}

void draw() { 
  background(255);
  int i;
  
  //boucle qui dessine les segments
  for(i=0;i<seg_vert.size();++i)
    seg_vert.get(i).draw(0);
  for(i=0;i<seg_horz.size();++i)
    seg_horz.get(i).draw(0);
  
  //boucle qui dessine les points d'intersection
  for(i=0;i<inters.size();++i){
   inters.get(i).draw(#ff0000); 
  }
  
}

//fonction qui génère les segments aléatoirement
public void generate(int nb, int lg, int variant){
  int m = nb/2;
  int min,i;
  
  //création segments verticaux;
  for(i=0;i<m;++i){
    min = (int)(random(height));
    seg_vert.add(new Segment(true,min,min+((int)(lg*(random(100-variant,100+variant)/100))),(int)random(width) ));
  }
  //création segments horizontaux
  for(i=m;i<nb;++i){
    min = (int)(random(width));
    seg_horz.add(new Segment(false,min,min+((int)(lg*(random(100-variant,100+variant)/100))),(int)random(height) ));
  }
}

//1ere approche avec l'algorithme en brut force
public void BrutForceAlgo(){
  int end;
  int timer = millis();
  Intersection temp;
  for(int i=0;i<seg_vert.size();++i){
      for(int j=0;j<seg_horz.size();++j){
          temp = seg_vert.get(i).hasIntersection(seg_horz.get(j));
          if(temp!=null)
              intersFB.add(temp);
      }
  }
  end = millis();
  System.out.println("Temps d'exécution pour le brutforce : "+(end-timer));
}

//2e approche avec la méthode "divide & conquer"
public void DNQAlgo(){
  //Entrée : Liste de segments horizontaux et verticaux
  //Sortie : liste des points d'intersection avec les segments associés pour chaque point
  
  int end;
  int timer=millis();
  //tri des segments verticaux selon l'axe des x dans l'ordre croissant
  Collections.sort(seg_vert,new Comparator<Segment>(){
    public int compare(Segment s1, Segment s2){
      return s1.p1.x < s2.p1.x ? -1 :
        s1.p1.x > s2.p1.x ? 1 : 0;
    }
  });

  for(int i=0;i<seg_horz.size();++i){
     //appel fco recursive
     rechIntersect(seg_horz.get(i),0,seg_vert.size()-1);
  }
  end = millis();
  
  System.out.println("Temps d'exécution pour DNQ : "+(end-timer));

}

//fonction recursive qui recherche des segments verticaux qui ont une intersection avec un segment horizontal donné
// les indices begin et end délimitent l'intervalle de segments verticaux triés qui sont testés
public void rechIntersect(Segment s, int begin, int end){
  
  //cas de fin, si l'ensemble ne comprend qu'un element, on effectue directement le test d'intersection
  if((end-begin)+1==1){
    Intersection res = seg_vert.get(begin).hasIntersection(s);
    if(res!=null)
      intersdnq.add(res);
  }
  else{
  
    //calcul du milieu de l'ensemble traité
    int milieu = (begin+end)/2;
    Segment smid = seg_vert.get(milieu);
    
    //si le segment vertical à cet indice est situé avant le segment horizontal (sur l'axe des x) passé en paramètre..
    if(smid.p1.x < s.p1.x){
      //on relance la fonction récursive sur l'intervalle de gauche [i, milieu]
      rechIntersect(s,milieu+1,end);
    }
    //de même si le segment vertical est situé après le segment horizontal (sur l'axe des x) passé en paramètre...
    else if(smid.p1.x > s.p2.x){
      //on relance la fonction récursive sur l'intervalle de droite [milieu+1,j]
        rechIntersect(s,begin,milieu);
    }
    //sinon, cela signifie que le segment vertical est situé entre le xmin et le xmax du segment horizontal et donc peut avoir une intersection
    //dans ce cas, on recherche de part et d'autre tous les segment verticaux adjacents à celui ci qui sont comprix entre xmin et xmax
    else{
       int left,right;
       //recherche des segments adjacents situés après xmin
    
       left=milieu-1;
       while( left>=0 && (seg_vert.get(left).p1.x >= s.p1.x) ){
         left--;
       }
       left++;
       
       //recherche des segments adjacents situés avant xmax
       right=milieu+1;
       while(right<seg_vert.size() && (seg_vert.get(right).p1.x <= s.p2.x)){
         right++;
       }
       right--;
       
       Intersection res;
       //Test d'intersection de tous les segments verticaux trouvés avec le segment horizontal et ajout au tableau de résultat si c'est le cas
       for(int i=left;i<=right;++i){
          res = seg_vert.get(i).hasIntersection(s);
          if(res!=null){
            intersdnq.add(res);
          }
       }
    }
  }
}

//Permet de trier la liste passé en paramètre pour le sweepline
public void sortArrayPoint(ArrayList<Point> liste){
 
  Collections.sort(liste, new Comparator<Point>(){
      @Override
      public int compare(Point p1, Point p2) {
          if(p1.y > p2.y)
            return -1;
          else if(p1.y == p2.y){
             if(p1.x < p2.x){
               return -1;
             }
             else if(p1.x == p2.x){
               
                Segment s1 = (Segment) p1.seg;
                Segment s2 = (Segment) p2.seg;
                if(s1 == s2){ //Si les deux points appartiennent au même segment
                   if(s1.type){ //Si le segment est vertical
                     if(s1.p2 == p1)
                       return -1;
                     else
                       return 1;
                   }
                   else{ //Sinon le segment est horizontal
                       if(s1.p1 == p1)
                         return -1;
                       else
                         return 1;
                   }
                }
                else if(s1.type && p1 == s1.p1 && !s2.type && p2 == s2.p1)
                  return 1;
                else if(s2.type && p2 == s2.p1 && !s1.type && p1 == s1.p1)
                  return -1;
                else if(!s1.type && p1 == s1.p2 && s2.type && p2 == s2.p2)
                   return 1; 
                else if(!s2.type && p2 == s2.p2 && s1.type && p1 == s1.p2)
                  return -1;
                else 
                  return 0;
               }
             else{
                return 1; 
             }
          }
          else{
            return 1;
          }
      }
    });
  
}

//Permet d'ajouter un point d'intersection dans la liste
public void ajoutPointIntersection(ArrayList<Point> liste, Point p){
 
  boolean ajout = false;
  
  //On parcourt la liste des points
  for(int i = 0; i < liste.size(); i++){
     
    Point pListe = liste.get(i);
     
     //Si le point en cours a un y plus faible que le point qu'on veut ajouter
     if(pListe.y < p.y){
        liste.add(i, p);
        ajout = true;
        break;
     }
     else if(pListe.y == p.y){
       if(pListe.x > p.x){ //Si le point en cours a un x plus fort que le point qu'on veut ajouter
          liste.add(i, p);
          ajout = true;
          break;
       }
       else if(pListe.x == p.x){
         
         if(pListe.seg instanceof Intersection){ //Si le point en cours est un point d'intersection
            liste.add(i, p);
            ajout = true;
            break;
         }
         else{ //Sinon c'est un point appartenant à un segment
           
           Intersection inter = (Intersection) p.seg;
           Segment s = (Segment)pListe.seg;
           
           //Ces tests permettent de savoir si l'intersection doit être mis avant ou après le point en cours
           if((inter.s1.type && inter.s1.p1 != pListe) || (inter.s2.type && inter.s2.p1 != pListe) || (!inter.s1.type && inter.s1.p2 != pListe) || (!inter.s2.type && inter.s2.p2 != pListe)){
             //on ajoute l'intersection
                liste.add(i, p);
                ajout = true;
                break; 
           }
          else if((s.type && pListe == s.p1) || (!s.type && pListe == s.p2)){
             liste.add(i, p);
              ajout = true;
              break; 
          }
            
         }
          
       }
     }
    
  }
  
  if(!ajout)
    liste.add(p);
  
}

//3e approche avec l'algorithme du sweepline
public void sweepline(){
  int end;
  int timer=millis();
 
  //Liste des segments
  ArrayList<Segment> sweepSegment = new ArrayList<Segment>();
  
  ArrayList<Point> sweepPoints = new ArrayList<Point>();
  
  //On remplit la liste des points
  for(int i = 0; i < seg_vert.size(); ++i){
    Point p1 = seg_vert.get(i).p1;
    Point p2 = seg_vert.get(i).p2;
    sweepPoints.add(p1);
    sweepPoints.add(p2);
  }
  for(int i = 0; i < seg_horz.size(); ++i){
    Point p1 = seg_horz.get(i).p1;
    Point p2 = seg_horz.get(i).p2;
    sweepPoints.add(p1);
    sweepPoints.add(p2);
  }
  
  //On trie la liste des points
  sortArrayPoint(sweepPoints);

  //Tant qu'on a des points
  while(sweepPoints.size() > 0){
    
    //On récupère le point actuel
    Point currentPoint = sweepPoints.get(0);
    
    //Si le point appartient à un segment
    if(currentPoint.seg instanceof Segment){
       
        //On récupère le segment
        Segment seg = (Segment) currentPoint.seg;
        
        //Si le segment n'est pas déjà présent dans la liste des segments
        if(sweepSegment.indexOf(seg) == -1){
          
          int indice = -1;
          
          //On détermine la position du segment dans la liste
          for(int i = 0; i < sweepSegment.size(); i++){
            if(sweepSegment.get(i).type == seg.type && sweepSegment.get(i).p1.x > seg.p1.x){
               indice = i;
               break;
            }
            else if(sweepSegment.get(i).type != seg.type && sweepSegment.get(i).p1.x >= seg.p1.x){
               indice = i;
               break;
            }
          }
          
          //On ajoute le segment dans la liste
          if(indice == -1){
            sweepSegment.add(seg);
            indice = sweepSegment.size()-1;
          }
          else
            sweepSegment.add(indice, seg);
          
          Intersection gauche = null;
          Intersection droite = null;
          
          if(indice < sweepSegment.size()-1){
            droite = seg.hasIntersection(sweepSegment.get(indice+1));
          }
            
          if(indice > 0){
            gauche = seg.hasIntersection(sweepSegment.get(indice-1));
          }
            
          //Si il y a un segment avant celui que l'on vient d'ajouter on test si il y a une intersection
          if(indice < sweepSegment.size()-1 && seg.type != sweepSegment.get(indice+1).type && droite != null){
            ajoutPointIntersection(sweepPoints, droite.inter);
            inters.add(droite);
          }
          
          //Si il y a un segment après celui que l'on vient d'ajouter on test si il y a une intersection
          if(indice > 0 && seg.type != sweepSegment.get(indice-1).type && gauche != null){
            ajoutPointIntersection(sweepPoints, gauche.inter);
            inters.add(gauche);
          }
          
        }
        else{ //Si le segment existe déjà
           
          int indice = sweepSegment.indexOf(seg);
          int indavant = indice-1;
          int indapres = indice+1;
           
          //Si le segment a un segment avant et après lui dans la liste on essaye de voir si il y a une intersection entre ces deux
          if(indavant >= 0 && indapres <= sweepSegment.size()-1 && sweepSegment.get(indavant).type != sweepSegment.get(indapres).type && sweepSegment.get(indavant).hasIntersection(sweepSegment.get(indapres)) != null){
            Intersection intersection = sweepSegment.get(indavant).hasIntersection(sweepSegment.get(indapres));
              
            boolean search = false;
            
            //On essaye de voir si l'intersection n'existe pas déjà
            for(int i = 0; i < inters.size(); i++){
             if((inters.get(i).s1 == intersection.s1 && inters.get(i).s2 == intersection.s2) || (inters.get(i).s2 == intersection.s1 && inters.get(i).s1 == intersection.s2))
               search = true;
            }
            
            //Si elle n'existe pas on l'ajoute
            if(!search){

              ajoutPointIntersection(sweepPoints, intersection.inter);
              inters.add(intersection); 
            
            }    
        
          }  
        //On supprime le segment
        sweepSegment.remove(indice);
        }
    }
    else{ //Si notre point actuel appartient à une intersection
      
      Intersection intersection = (Intersection) currentPoint.seg;
      
      int i = sweepSegment.indexOf(intersection.s1);
      int j = sweepSegment.indexOf(intersection.s2);
       
      //On vérifie que la position de notre i est inférieur à j
      if(j < i){
         int temp = i;
         i = j;
         j = temp;
      }
       
      //On change la position des segments de l'intersection si le segment situé à gauche est horizontal
      if(!sweepSegment.get(i).type){
        Collections.swap(sweepSegment, i, j);
      }
      
      //Si il y a un segment à gauche du segment à la position i on essaye de voir si il y a une intersection
      if(i > 0 && sweepSegment.get(i).type != sweepSegment.get(i-1).type && sweepSegment.get(i).hasIntersection(sweepSegment.get(i-1)) != null){
        
          Intersection interI = sweepSegment.get(i).hasIntersection(sweepSegment.get(i-1));
          
          boolean search = false;
            
            //On essaye de voir si l'intersection n'existe pas déjà
            for(int e = 0; e < inters.size(); e++){
             if((inters.get(e).s1 == interI.s1 && inters.get(e).s2 == interI.s2) || (inters.get(e).s2 == interI.s1 && inters.get(e).s1 == interI.s2)){
               search = true;
             }
            }
            
            //Si elle n'existe pas on l'ajoute
            if(!search){
              ajoutPointIntersection(sweepPoints, interI.inter);
              inters.add(interI);
            }
      }
      if(j > 0 && sweepSegment.get(j).type != sweepSegment.get(j-1).type && sweepSegment.get(j).hasIntersection(sweepSegment.get(j-1)) != null){
        
         Intersection interJ = sweepSegment.get(j).hasIntersection(sweepSegment.get(j-1));
         
         boolean search = false;
            
          //On essaye de voir si l'intersection existe déjà dans la liste
          for(int e = 0; e < inters.size(); e++){
             if((inters.get(e).s1 == interJ.s1 && inters.get(e).s2 == interJ.s2) || (inters.get(e).s2 == interJ.s1 && inters.get(e).s1 == interJ.s2))
               search = true;
          }
            
          //Si elle n'existe pas on l'ajoute
          if(!search){
            ajoutPointIntersection(sweepPoints, interJ.inter);
            inters.add(interJ);
          }
      }
      if(i < sweepSegment.size()-1 && sweepSegment.get(i).type != sweepSegment.get(i+1).type && sweepSegment.get(i).hasIntersection(sweepSegment.get(i+1)) != null){
        
          Intersection interI = sweepSegment.get(i).hasIntersection(sweepSegment.get(i+1));
          
          boolean search = false;
            
            //On essaye de voir si l'intersection n'existe pas déjà
            for(int e = 0; e < inters.size(); e++){
             if((inters.get(e).s1 == interI.s1 && inters.get(e).s2 == interI.s2) || (inters.get(e).s2 == interI.s1 && inters.get(e).s1 == interI.s2)){
               search = true;
             }
            }
            
            //Si elle n'existe pas on l'ajoute
            if(!search){
              ajoutPointIntersection(sweepPoints, interI.inter);
              inters.add(interI);
          }
      }
      //Sil il y a un segment à droite du segment en position j on essaye de voir si il y a une intersection entre eux
      if(j < sweepSegment.size()-1 && sweepSegment.get(j).type != sweepSegment.get(j+1).type && sweepSegment.get(j).hasIntersection(sweepSegment.get(j+1)) != null){
        
         Intersection interJ = sweepSegment.get(j).hasIntersection(sweepSegment.get(j+1));
         
         boolean search = false;
            
          //On essaye de voir si l'intersection existe déjà dans la liste
          for(int e = 0; e < inters.size(); e++){
             if((inters.get(e).s1 == interJ.s1 && inters.get(e).s2 == interJ.s2) || (inters.get(e).s2 == interJ.s1 && inters.get(e).s1 == interJ.s2))
               search = true;
          }
            
          //Si elle n'existe pas on l'ajoute
          if(!search){
            ajoutPointIntersection(sweepPoints, interJ.inter);
            inters.add(interJ);
          }
         
      }
      
    }
    
    //On supprime le point actuel de notre liste et on trie la liste
    sweepPoints.remove(currentPoint);
   
    
  }
  end = millis();
  System.out.println("Temps d'exécution pour sweep-line : "+(end-timer));
}



class Intersection{

  Segment s1;
  Segment s2;
  
  Point inter;

  public Intersection(Segment s1, Segment s2, int x, int y){
    this.s1 = s1;
    this.s2 = s2;
    inter = new Point(x, y, this);
  }
  
  public void draw(int clr){
    stroke(clr);
    ellipse(inter.x, inter.y, 5, 5);
    noStroke();
  }
  
}
  
class Point{
  int x;
  int y;
  Object seg;
  
  public Point(int x, int y, Object seg){
    this.x = x;
    this.y = y;
    this.seg = seg;
  }

}

class Segment{

  Point p1;
  Point p2;
  boolean type;
  
  /**
  *  @type definit dans quel sens est généré le segment.
  *   Si type == true, le segment est vertical
  *   sinon il sera vertical
  *  Les autres paramètres définissent les coordonnées du segment
  *  @a et @b sont les coordonnées qui sont différente entre les extrémité du segment
  *  et @c sera la coordonnée identique
  *  Dans le cas où le segment est vertical, @a et @b seront respectivement les coordonnées en y des extrémités, et @c sera affecté à la coordonnée x.
  *  Inversement, pour un segment horizontal @a et @b seront les coordonnées en x, et @c la coordonnée en y.
  * on partira du principe que x1 sera toujours inférieur à x2 et y1 sera inférieur à y2
  */
  public Segment(boolean type, int a, int b, int c){
    this.type = type;
    if(type){
      p1 = new Point(c, a, this);
      p2 = new Point(c, b, this);
    }
    else{
      p1 = new Point(a, c, this);
      p2 = new Point(b, c, this);
    }
  }
  
  public Intersection hasIntersection(Segment s){

    Intersection result = null;
    
    if(this.type){ //Segment vertical
      
      if(s.p1.x <= this.p1.x && this.p1.x <= s.p2.x && this.p1.y <= s.p1.y && s.p1.y <= this.p2.y){
        result = new Intersection(this, s, this.p1.x, s.p1.y);
      }
    
    } //Segment horizontal
    else if(this.p1.x <= s.p1.x && s.p1.x <= this.p2.x && s.p1.y <= this.p1.y && this.p1.y <= s.p2.y){ 
      result = new Intersection(this, s, s.p1.x, this.p1.y);
    }
    
    return result;

  }

  /**
  * fonction qui affiche le segment à l'écran
  * @clr la couleur à utiliser pour dessiner le segment
  *
  */
  public void draw(int clr){
    stroke(clr);
    line(p1.x,p1.y,p2.x,p2.y);
    noStroke();
   }
}